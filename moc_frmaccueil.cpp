/****************************************************************************
** Meta object code from reading C++ file 'frmaccueil.h'
**
** Created: Sun Apr 6 15:06:51 2008
**      by: The Qt Meta Object Compiler version 59 (Qt 4.3.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "frmaccueil.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'frmaccueil.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.3.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_Qaccueil[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      17,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      21,    9,    9,    9, 0x08,
      32,    9,    9,    9, 0x08,
      42,    9,    9,    9, 0x08,
      52,    9,    9,    9, 0x08,
      63,    9,    9,    9, 0x08,
      70,    9,    9,    9, 0x08,
      78,    9,    9,    9, 0x08,
      85,    9,    9,    9, 0x08,
      93,    9,    9,    9, 0x08,
     101,    9,    9,    9, 0x08,
     113,    9,    9,    9, 0x08,
     125,    9,    9,    9, 0x08,
     139,    9,    9,    9, 0x08,
     151,    9,    9,    9, 0x08,
     164,    9,    9,    9, 0x08,
     173,    9,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Qaccueil[] = {
    "Qaccueil\0\0cmdajout()\0cmdmodif()\0"
    "cmdsupp()\0cmdsave()\0cmdannul()\0cmdn()\0"
    "cmdnn()\0cmdp()\0cmdpp()\0table()\0"
    "recherche()\0actualise()\0attpresence()\0"
    "modhakara()\0frmndirect()\0conpar()\0"
    "indhar()\0"
};

const QMetaObject Qaccueil::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Qaccueil,
      qt_meta_data_Qaccueil, 0 }
};

const QMetaObject *Qaccueil::metaObject() const
{
    return &staticMetaObject;
}

void *Qaccueil::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Qaccueil))
	return static_cast<void*>(const_cast< Qaccueil*>(this));
    if (!strcmp(_clname, "Ui::frmaccueil"))
	return static_cast< Ui::frmaccueil*>(const_cast< Qaccueil*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Qaccueil::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: cmdajout(); break;
        case 1: cmdmodif(); break;
        case 2: cmdsupp(); break;
        case 3: cmdsave(); break;
        case 4: cmdannul(); break;
        case 5: cmdn(); break;
        case 6: cmdnn(); break;
        case 7: cmdp(); break;
        case 8: cmdpp(); break;
        case 9: table(); break;
        case 10: recherche(); break;
        case 11: actualise(); break;
        case 12: attpresence(); break;
        case 13: modhakara(); break;
        case 14: frmndirect(); break;
        case 15: conpar(); break;
        case 16: indhar(); break;
        }
        _id -= 17;
    }
    return _id;
}
