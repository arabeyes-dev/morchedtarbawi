/* Morched Tarbawi
* Copyright (c) 2007 Wathek LOUED.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef QNATTPRES_Q
#define QNATTPRES_H

#include "ui_frmattpres.h"
#include <QtSql>

class Qaccueil;

class frmapercu;

class frmattpres: public QDialog, public Ui::frmattpres
{
Q_OBJECT

public:
        frmattpres(QSqlQueryModel *model, QWidget *parent = 0 );

private slots:
        void appliquer();

private:
        Qaccueil *frmaccueil;
	frmapercu *fapercu;
        QSqlQueryModel *m_model;
//        QSqlQueryModel *model2;
};
#endif

