/* Morched Tarbawi
 * Copyright (c) 2007 Wathek LOUED.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "frmrecherche.h"
#include "frmaccueil.h"
#include <QtSql>
#include <QLocale>
#include <QString>
#include <QMessageBox>

QRecherche::QRecherche(QSqlQueryModel *model,QWidget *parent)
        :QDialog(parent), m_model(model)
{
	setupUi(this);	
	
	connect(pushButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(radioButton, SIGNAL(clicked()), this, SLOT(opt1()));
	connect(radioButton_2, SIGNAL(clicked()), this, SLOT(opt2()));
	connect(radioButton_3, SIGNAL(clicked()), this, SLOT(opt3()));
	connect(pushButton_2, SIGNAL(clicked()), this, SLOT(cmdrecherche()));
	
	lineEdit->setVisible(FALSE);
	dateEdit->setVisible(FALSE);
	comboBox->setVisible(FALSE);
	dateEdit->setCalendarPopup(1);
	dateEdit->setDisplayFormat("dd-MM-yyyy");
	
        QSqlQuery query;
        query.prepare("SELECT DISTINCT classe from Table1");
        query.exec();
        query.first();
        for (int i=1; i<=m_model->rowCount()-1; i++)
                {
			comboBox->addItem(query.value(0).toString());
                        query.next();

                }
	pushButton_2->setDefault(1);
	radioButton->setChecked(1);
	opt1();
}

void QRecherche::opt1()
{
	lineEdit->setVisible(TRUE);
	dateEdit->setVisible(FALSE);
	comboBox->setVisible(FALSE);
}

void QRecherche::opt2()
{
	lineEdit->setVisible(FALSE);
	dateEdit->setVisible(TRUE);
	comboBox->setVisible(FALSE);
}

void QRecherche::opt3()
{
	lineEdit->setVisible(FALSE);
	dateEdit->setVisible(FALSE);
	comboBox->setVisible(TRUE);
}

void QRecherche::cmdrecherche()
{
	if (radioButton->isChecked()) { m_model->setQuery("select * from Table1 where Nom like '%" + lineEdit->text() + "%'"); this->close();}
	else if (radioButton_2->isChecked()) { m_model->setQuery("select * from Table1 where date='" + dateEdit->date().toString("yyyy-MM-dd") + "'"); this->close();}
	else if (radioButton_3->isChecked()) { m_model->setQuery("select * from Table1 where classe='" + comboBox->itemText(comboBox->currentIndex()) + "'"); this->close();}
}
