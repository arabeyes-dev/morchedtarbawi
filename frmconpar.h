/* Morched Tarbawi
* Copyright (c) 2007 Wathek LOUED.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef frmconpar_H
#define frmconpar_H

#include "ui_frmconpar.h"
#include <QtSql>

class Qaccueil;

class frmconpar: public QDialog, public Ui::frmconpar
{
Q_OBJECT

public:
        frmconpar(QSqlQueryModel *model, QWidget *parent = 0 );

private slots:
        void appliquer();

private:
        Qaccueil *frmaccueil;
        //QSqlQueryModel &model;
        QSqlQueryModel *m_model;
};
#endif
