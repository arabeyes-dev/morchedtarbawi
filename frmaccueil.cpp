/* Morched Tarbawi
 * Copyright (c) 2007 Wathek LOUED.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "frmaccueil.h"
#include "frmrecherche.h"
#include "frmdirect.h"
#include "frmattpres.h"
#include "frmmodhakara.h"
#include "frmconpar.h"
#include "frmindhar.h"

#include <QtSql>
#include <QLocale>
#include <QString>
#include <QMessageBox>
#include <QFontDatabase>
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QDesktopWidget>

	QString snom,sdate,sclasse;

Qaccueil::Qaccueil(QWidget *parent)
	:QMainWindow(parent)
{
	setupUi(this);
//	QFontDatabase database;
	//database.addApplicationFont("fonts/ae_AlMohanad.ttf");
	//QFontDatabase::addApplicationFont("ae_AlMohanad.ttf");	
	QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
	db.setHostName("localhost");
	db.setDatabaseName("Idarat");
	db.setUserName("root");
	db.setPassword("");
	bool ok = db.open();
//----------------------------------------
//	model = new QSqlTableModel(this);
//	model->setTable("Table1");
//	model->setEditStrategy(QSqlTableModel::OnManualSubmit);
//	model->select();
//----------------------------------------	
//	while (model->columnCount() > 3) 
//		model->removeColumn(3);
//		model->setHiddenColumn(3);
//	QSqlQueryModel * model = new QSqlQueryModel;
	

	model = new QSqlQueryModel(this);
	model->setQuery("select * from Table1 ORDER BY classe, Nom");

	model->setHeaderData(0, Qt::Horizontal, trUtf8("اسم النلميذ"));
	model->setHeaderData(1, Qt::Horizontal, trUtf8("تاريخ الولادة"));
	model->setHeaderData(2, Qt::Horizontal, trUtf8("القسم"));	
	tableView->setModel(model);
	tableView->setColumnWidth(0, 250);
	tableView->setColumnWidth(1, 100);
	tableView->setColumnWidth(2, 200);
	tableView->setColumnHidden(3, 1);
	tableView->setColumnHidden(4, 1);

	B4->setEnabled(FALSE);
	B5->setEnabled(FALSE);
	tableView->show();

	connect(B8, SIGNAL(clicked()), qApp, SLOT(closeAllWindows()));
	connect(B1, SIGNAL(clicked()), this, SLOT(cmdajout()));
	connect(B2, SIGNAL(clicked()), this, SLOT(cmdmodif()));
	connect(B4, SIGNAL(clicked()), this, SLOT(cmdsave()));
	connect(B5, SIGNAL(clicked()), this, SLOT(cmdannul()));
	connect(B3, SIGNAL(clicked()), this, SLOT(cmdsupp()));
	connect(B7, SIGNAL(clicked()), this, SLOT(actualise()));
	connect(pushButton_10, SIGNAL(clicked()), this, SLOT(cmdpp()));
	connect(pushButton_8, SIGNAL(clicked()),this, SLOT(cmdn()));
	connect(pushButton_9, SIGNAL(clicked()),this, SLOT(cmdnn()));
	connect(pushButton_11, SIGNAL(clicked()),this,SLOT(cmdp()));
	connect(tableView->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),this,SLOT(table()));
	connect(B6, SIGNAL(clicked()), this, SLOT(recherche()));
	connect(action, SIGNAL(triggered()), this, SLOT(attpresence()));
	connect(action_9, SIGNAL(triggered()), this, SLOT(frmndirect()));
	connect(action_2, SIGNAL(triggered()), this, SLOT(modhakara()));
	connect(action_3, SIGNAL(triggered()), this, SLOT(conpar()));
	connect(action_4, SIGNAL(triggered()), this, SLOT(indhar()));
	//connect(B7, SIGNAL(clicked()), this, SLOT(test()));

	lineEdit->setText(model->record(0).value("Nom").toString().trimmed());
	dateEdit->setDate(QDate::fromString(model->record(0).value("date").toString().trimmed(), Qt::ISODate));
	lineEdit_3->setText(model->record(0).value("classe").toString().trimmed());
	lineEdit_4->setText(model->record(0).value("adresse").toString().trimmed());
	lineEdit_5->setText(model->record(0).value("parent").toString().trimmed());
	tableView->selectRow(0);
	dateEdit->setReadOnly(TRUE);
	dateEdit->setDisplayFormat("dd-MM-yyyy");
	QDesktopWidget screen;
	this->move((screen.screenGeometry().width()- this->geometry().width())/2.0 , (screen.screenGeometry().height()-this->geometry().height())/2.0);

}

//--------------- Boutton Ajouter -----------------------------------------
void Qaccueil::cmdajout() 
{
	am = 0;
	B1->setEnabled(FALSE);
	B2->setEnabled(FALSE);
	B3->setEnabled(FALSE);
	B4->setEnabled(TRUE);
	B5->setEnabled(TRUE);
	B6->setEnabled(FALSE);
	B7->setEnabled(FALSE);
	
	lineEdit->setReadOnly(FALSE);
	dateEdit->setReadOnly(FALSE);
	dateEdit->setCalendarPopup(TRUE);	
	lineEdit_3->setReadOnly(FALSE);
	lineEdit_4->setReadOnly(FALSE);
	lineEdit_5->setReadOnly(FALSE);

	lineEdit->clear();
	dateEdit->clear();
	lineEdit_3->clear();
	lineEdit_4->clear();
	lineEdit_5->clear();
	
	lineEdit->setFocus();

}
//------------ Boutton Modifier ------------------------------------------
void Qaccueil::cmdmodif() 
{
	am = 1;
	B1->setEnabled(FALSE);
	B2->setEnabled(FALSE);
	B3->setEnabled(FALSE);
	B4->setEnabled(TRUE);
	B5->setEnabled(TRUE);
	B6->setEnabled(FALSE);
	B7->setEnabled(FALSE);

	lineEdit->setReadOnly(FALSE);
	dateEdit->setReadOnly(FALSE);
	dateEdit->setCalendarPopup(TRUE);
	lineEdit_3->setReadOnly(FALSE);
	lineEdit_4->setReadOnly(FALSE);
	lineEdit_5->setReadOnly(FALSE);

	lineEdit->setFocus();
}
//------------ Boutton Enregistrer---------------------------------------
void Qaccueil::cmdsave() 
{
	//  Creation d'un nouvel enregistrement si am =0 ce qui veut dire
	//  le boutton Ajouter a ete cliqué
	if (am ==0 )
	{
		B1->setEnabled(TRUE);
		B2->setEnabled(TRUE);
		B3->setEnabled(TRUE);
		B4->setEnabled(FALSE);
		B5->setEnabled(FALSE);
		B6->setEnabled(TRUE);
		B7->setEnabled(TRUE);

		lineEdit->setReadOnly(TRUE);
		dateEdit->setReadOnly(FALSE);
		dateEdit->setCalendarPopup(FALSE);
		lineEdit_3->setReadOnly(TRUE);
		lineEdit_4->setReadOnly(TRUE);
		lineEdit_5->setReadOnly(TRUE);
		
		QSqlQuery query;
		query.prepare("INSERT INTO Table1 (Nom, date, classe, adresse, parent) "
	                      "VALUES (?, ?, ?, ?, ?)");
		query.bindValue(0, lineEdit->text());
	        query.bindValue(1, dateEdit->text());
        	query.bindValue(2, lineEdit_3->text());
		query.bindValue(3, lineEdit_4->text());
		query.bindValue(4, lineEdit_5->text());
	        query.exec();
	}
	// Modification de l'enregistrement selectionné ce qui veut dire
	// le boutton Modifier a ete cliqué
	else if (am==1)
	{
		B1->setEnabled(TRUE);
                B2->setEnabled(TRUE);
                B3->setEnabled(TRUE);
                B4->setEnabled(FALSE);
                B5->setEnabled(FALSE);
                B6->setEnabled(TRUE);
                B7->setEnabled(TRUE);

                lineEdit->setReadOnly(TRUE);
                dateEdit->setReadOnly(TRUE);
		dateEdit->setCalendarPopup(FALSE);
                lineEdit_3->setReadOnly(TRUE);
                lineEdit_4->setReadOnly(TRUE);
                lineEdit_5->setReadOnly(TRUE);

        	QModelIndex rec = tableView->currentIndex();

                QSqlQuery query;
                query.prepare("UPDATE Table1 set Nom=:nom, date=:date, classe=:classe, adresse=:adresse, parent=:parent where Nom=:nomanc and classe=:classeanc");
                query.bindValue(":nom", lineEdit->text());
                query.bindValue(":date", dateEdit->text());
                query.bindValue(":classe", lineEdit_3->text());
                query.bindValue(":adresse", lineEdit_4->text());
                query.bindValue(":parent", lineEdit_5->text());
		query.bindValue(":nomanc", model->record(rec.row()).value("Nom").toString());
		query.bindValue(":classeanc", model->record(rec.row()).value("classe").toString());
                query.exec();
	}	
	// Actualisation de l'affichage
	model->setQuery("select * from Table1 ORDER BY classe, Nom");
}
// ---------- Boutton Annuler -------------
void Qaccueil::cmdannul() 
{
	B1->setEnabled(TRUE);
	B2->setEnabled(TRUE);
	B3->setEnabled(TRUE);
	B4->setEnabled(FALSE);
	B5->setEnabled(FALSE);
	B6->setEnabled(TRUE);
	B7->setEnabled(TRUE);
	
	lineEdit->setReadOnly(TRUE);
	dateEdit->setReadOnly(TRUE);
	dateEdit->setCalendarPopup(FALSE);
	lineEdit_3->setReadOnly(TRUE);
	lineEdit_4->setReadOnly(TRUE);
	lineEdit_5->setReadOnly(TRUE);
	
	model->setQuery("select * from Table1 ORDER BY classe, Nom");
}
// -------- Boutton de navigation revenir au debut----------
void Qaccueil::cmdpp()
{
	tableView->selectRow(0);

        lineEdit->setText(model->record(0).value("Nom").toString());
        dateEdit->setDate(QDate::fromString(model->record(0).value("date").toString(), Qt::ISODate));
        lineEdit_3->setText(model->record(0).value("classe").toString());
        lineEdit_4->setText(model->record(0).value("adresse").toString());
        lineEdit_5->setText(model->record(0).value("parent").toString());

	tableView->setFocus();

}
// ------- Boutton de navigation Suivant ----------
void Qaccueil::cmdn()
{
	QModelIndex rec = tableView->currentIndex();	
	int n;
	n = rec.row();


	if ( n < model->rowCount()-1 ) n = rec.row()+1;
	else 	n = (model->rowCount())-1;
	
	tableView->selectRow(n);

        lineEdit->setText(model->record(n).value("Nom").toString());
        dateEdit->setDate(QDate::fromString(model->record(n).value("date").toString(), Qt::ISODate));
        lineEdit_3->setText(model->record(n).value("classe").toString());
        lineEdit_4->setText(model->record(n).value("adresse").toString());
        lineEdit_5->setText(model->record(n).value("parent").toString());
	tableView->setFocus();
}
// ------ Boutton de navigation aller a la fin -----------
void Qaccueil::cmdnn()
{
	tableView->selectRow(model->rowCount()-1);

        lineEdit->setText(model->record(model->rowCount()-1).value("Nom").toString());
        dateEdit->setDate(QDate::fromString(model->record(model->rowCount()-1).value("date").toString(), Qt::ISODate));
        lineEdit_3->setText(model->record(model->rowCount()-1).value("classe").toString());
        lineEdit_4->setText(model->record(model->rowCount()-1).value("adresse").toString());
        lineEdit_5->setText(model->record(model->rowCount()-1).value("parent").toString());

	tableView->setFocus();

}
// ------- Boutton de navigation precedent--------------
void Qaccueil::cmdp()
{
	QModelIndex rec = tableView->currentIndex();

        int n;
        n = rec.row();

        if ( n == 0 ) n = 0;
        else    n = rec.row()-1;
	
	tableView->selectRow(n);

        lineEdit->setText(model->record(n).value("Nom").toString());
        dateEdit->setDate(QDate::fromString(model->record(n).value("date").toString(), Qt::ISODate));
        lineEdit_3->setText(model->record(n).value("classe").toString());
        lineEdit_4->setText(model->record(n).value("adresse").toString());
        lineEdit_5->setText(model->record(n).value("parent").toString());
	
	tableView->setFocus();
}
// -------- Boutton de suppression ----------------------
void Qaccueil::cmdsupp()
{
	QMessageBox msgbox;
	QPushButton *oui = msgbox.addButton(trUtf8("نعم"), QMessageBox::YesRole);
	QPushButton *non = msgbox.addButton(trUtf8("لا"), QMessageBox::NoRole);
	msgbox.setText(trUtf8("هل تريد فعلا القيام بالحذف ؟"));
	msgbox.exec();

	if (msgbox.clickedButton()==oui) {
		QSqlQuery query;
		query.prepare("DELETE FROM Table1 WHERE Nom=:nom and classe=:classe");
	        query.bindValue(":nom", lineEdit->text());
	        query.bindValue(":classe", lineEdit_3->text());
	        query.exec();
        	model->setQuery("select * from Table1 ORDER BY classe, Nom");
	}

}

void Qaccueil::table()
{
	QModelIndex rec = tableView->currentIndex();

	lineEdit->setText(model->record(rec.row()).value("Nom").toString());
        dateEdit->setDate(QDate::fromString(model->record(rec.row()).value("date").toString(), Qt::ISODate));
        lineEdit_3->setText(model->record(rec.row()).value("classe").toString());
        lineEdit_4->setText(model->record(rec.row()).value("adresse").toString());
        lineEdit_5->setText(model->record(rec.row()).value("parent").toString());

}

void Qaccueil::recherche()
{
	tableView->reset();
	actualise();
	cmdpp();
	frmrecherche = new QRecherche(model);
	frmrecherche->setModal(1);
	frmrecherche->show();
}

void Qaccueil::actualise()
{
	tableView->reset();
        model->setQuery("select * from Table1 ORDER BY classe, Nom");
	cmdpp();
}

void Qaccueil::frmndirect()
{
	fdirect = new frmdirect(model);
	fdirect->setModal(1);
	fdirect->show();
}

void Qaccueil::attpresence()
{


	snom = lineEdit->text();
	sdate = dateEdit->date().toString("yyyy-MM-dd");
	sclasse = lineEdit_3->text();

	fattpres = new frmattpres(model);
	fattpres->setModal(1);
	fattpres->show();

}

void Qaccueil::modhakara()
{
	fmodhakara = new frmmodhakara(model);
	fmodhakara->setModal(1);
	fmodhakara->show();
}

void Qaccueil::conpar()
{
	fconpar = new frmconpar(model);
	fconpar->setModal(1);
	fconpar->show();
}

void Qaccueil::indhar()
{
	findhar = new frmindhar(model);
	findhar->setModal(1);
	findhar->show();
}
