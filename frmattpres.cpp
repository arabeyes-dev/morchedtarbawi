/* Morched Tarbawi
* Copyright (c) 2007 Wathek LOUED.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "frmattpres.h"
#include "frmaccueil.h"
#include "frmapercu.h"
#include <QtSql>
//#include <QLocale>
//#include <QString>
#include <QPrinter>
#include <QPainter>
#include <QPixmap>

frmattpres::frmattpres(QSqlQueryModel *model,QWidget *parent)
        :QDialog(parent), m_model(model)
{
        setupUi(this);
        connect(pushButton_2, SIGNAL(clicked()), this, SLOT(close()));
        connect(pushButton, SIGNAL(clicked()), this, SLOT(appliquer()));

}

void frmattpres::appliquer()
{

		fapercu = new frmapercu(this);
//		fapercu->show();

//        QModelIndex rec = Qaccueil.tableView->currentIndex();
	extern QString snom, sdate, sclasse;

//	fapercu = new frmapercu(this);
//	fapercu->show();

QPrinter printer(QPrinter::HighResolution);
printer.setOutputFileName("print.pdf");
QPainter painter;
//QPixmap pix(printer.paperRect().width(), printer.paperRect().height());
//painter.begin(&printer);
painter.begin(&printer);

painter.setFont(QFont("Rasheeq", 11));
painter.drawText(6600, 300, 2400, 1300, Qt::AlignCenter , trUtf8("الجمهورية التونسية\nوزارة التربية و التكوين\nالإدارة الجهوية للتعليم ببن عروس\nمعهد إبن رشيق بالزهراء"));

QSqlQuery query;
query.exec("select * from houdour");
query.first();
QString num = query.value(0).toString();
QFont fnum("Rasheeq", 11, QFont::Bold);
painter.setFont(fnum);
painter.drawText(500, 500, 8500, 400, Qt::AlignLeft, num);

int newnum;
newnum = num.toInt() + 1;
num = QString("%1").arg(newnum);
for (int i=0; i<=(10 - num.length()); i++) num.prepend("0");
query.prepare("Update houdour set num=:num");
query.bindValue(":num", num);
query.exec();


QFont tit("Rasheeq", 25, QFont::Bold);
painter.setFont(tit);
painter.drawText(0, 2200, 9000, 600, Qt::AlignCenter, trUtf8("شهــادة حـضور"));
painter.setFont(QFont("Rasheeq", 15));

painter.drawText(7500, 3200, 1500, 400, Qt::AlignRight, trUtf8(": يـشـهد الـمدير"));
//---------- Retrieving the director name -----------------
//QSqlQuery query;
query.exec("select * from ndirect");
query.first();
QString directeur = query.value(0).toString();
//---------------------------------------------------------
painter.drawText(400, 3200, 7000, 400, Qt::AlignRight, directeur);

painter.drawText(7600, 3700, 1400, 400, Qt::AlignRight, trUtf8(": (أن التلميذ (ة"));
painter.drawText(400, 3700, 7200, 400, Qt::AlignRight, snom);

painter.drawText(7800, 4200, 1200, 400, Qt::AlignRight, trUtf8(" (المولود (ة" ));
painter.drawText(7450, 4200, 500, 400, Qt::AlignRight, trUtf8(": في"));
painter.drawText(500, 4200, 7000, 400, Qt::AlignRight, sdate);

painter.drawText(7600, 4700, 1400, 400, Qt::AlignRight, trUtf8(" (مـــرســم (ة")); 
painter.drawText(5820, 4700, 1800, 400, Qt::AlignRight, trUtf8("/  بالـــمدرســـــة"));
painter.drawText(3700, 4700, 2000, 400, Qt::AlignRight, trUtf8(" (المعــهد الـمـذكور (ة"));
painter.drawText(10, 4700, 3600, 400, Qt::AlignRight, trUtf8("(أعــــلاه و يـــزاول دراســتـــه (هــا"));

painter.drawText(8500, 5200, 500, 400, Qt::AlignRight, trUtf8(": بــ"));
painter.drawText(1500, 5200, 7000, 400, Qt::AlignRight, sclasse);

painter.drawText(5300, 5700, 3700, 400, Qt::AlignRight, trUtf8("(سلـمت هذه الشهادة بطلب من المعني (ة"));
painter.drawText(4100, 5700, 1300, 400, Qt::AlignRight, trUtf8(": بالأمر قصد"));

painter.drawText(500, 6200, 8500, 400, Qt::AlignRight, comboBox->itemText(comboBox->currentIndex())); 

painter.drawText(2000, 8500, 7000, 400, Qt::AlignLeft, trUtf8(": حرر بالزهراء في"));
painter.drawText(0, 8520, 1900, 400, Qt::AlignRight, QDate::currentDate().toString("yyyy-MM-dd"));

painter.drawText(0, 8950, 3900, 400, Qt::AlignCenter, trUtf8("الإمضاء و الختم"));
painter.drawText(0, 9320, 3900, 400, Qt::AlignCenter, trUtf8("(المدير (ة"));



//qDebug() << painter.viewport();


//for (int page = 0; page < numberOfPages; ++page) {
//      if (page != lastPage)
//              printer.newPage();
//}
painter.end();
//fapercu->label->setPixmap(pix);
//fapercu->show();
this->close();
}

