-- phpMyAdmin SQL Dump
-- version 2.10.3deb1ubuntu0.1
-- http://www.phpmyadmin.net
-- 
-- Serveur: localhost
-- Généré le : Mer 06 Février 2008 à 21:44
-- Version du serveur: 5.0.45
-- Version de PHP: 5.2.3-1ubuntu6.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de données: `Idarat`
-- 

-- --------------------------------------------------------

-- 
-- Structure de la table `houdour`
-- 

CREATE TABLE IF NOT EXISTS `houdour` (
  `num` text collate utf8_unicode_ci NOT NULL,
  `annee` text collate utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `houdour`
-- 

INSERT INTO `houdour` (`num`, `annee`) VALUES 
('000001', '');

-- --------------------------------------------------------

-- 
-- Structure de la table `ndirect`
-- 

CREATE TABLE IF NOT EXISTS `ndirect` (
  `nom` text collate utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- 
-- Contenu de la table `ndirect`
-- 

INSERT INTO `ndirect` (`nom`) VALUES 
('إسم المدير');

-- --------------------------------------------------------

-- 
-- Structure de la table `Table1`
-- 

CREATE TABLE IF NOT EXISTS `Table1` (
  `Nom` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `classe` text character set utf8 collate utf8_unicode_ci NOT NULL,
  `parent` text character set utf8 collate utf8_unicode_ci,
  `adresse` longtext character set utf8 collate utf8_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


