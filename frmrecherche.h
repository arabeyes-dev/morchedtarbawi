/* Morched Tarbawi
 * Copyright (c) 2007 Wathek LOUED.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/


#ifndef QRecherche_H
#define QRecherche_H

#include "ui_frmrecherche.h"
#include <QtSql>

class Qaccueil;

class QRecherche: public QDialog, public Ui::frmrecherche
{
Q_OBJECT

public:
        QRecherche(QSqlQueryModel *model, QWidget *parent = 0 );

private slots:
	void opt1();
	void opt2();
	void opt3();
	void cmdrecherche();

private:
	Qaccueil *frmaccueil;
	//QSqlQueryModel &model;
	QSqlQueryModel *m_model;
};
#endif
