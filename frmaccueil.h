/* Morched Tarbawi
 * Copyright (c) 2007 Wathek LOUED.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef QACCUEIL_H
#define QACCUEIL_H

#include "ui_accueil.h"
#include <QtSql>

class QRecherche;

class frmdirect;

class frmattpres;

class frmmodhakara;

class frmconpar;

class frmindhar;

class Qaccueil: public QMainWindow, public Ui::frmaccueil
{
Q_OBJECT

public:
        Qaccueil( QWidget *parent = 0);
        
private slots:
		void cmdajout();
		void cmdmodif();
		void cmdsupp();
		void cmdsave();
		void cmdannul();
		void cmdn();
		void cmdnn();
		void cmdp();
		void cmdpp();
		void table();
		//void test();
		void recherche();
		void actualise();
		void attpresence();
		void modhakara();
		void frmndirect();
		void conpar();
		void indhar();
private:
		QRecherche *frmrecherche;
		frmdirect *fdirect;
		frmattpres *fattpres;
		frmmodhakara *fmodhakara;
		frmconpar *fconpar;
		frmindhar *findhar;
		QSqlQueryModel *model;
//		frmrecherche = new QRecherche;
		int am;
};

#endif
