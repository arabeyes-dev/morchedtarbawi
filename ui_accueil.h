/********************************************************************************
** Form generated from reading ui file 'accueil.ui'
**
** Created: Sun Apr 6 15:06:33 2008
**      by: Qt User Interface Compiler version 4.3.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_ACCUEIL_H
#define UI_ACCUEIL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDateEdit>
#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QTableView>
#include <QtGui/QWidget>

class Ui_frmaccueil
{
public:
    QAction *action;
    QAction *action_2;
    QAction *action_3;
    QAction *action_4;
    QAction *action_5;
    QAction *action_6;
    QAction *action_7;
    QAction *action_8;
    QAction *action_9;
    QWidget *centralwidget;
    QGroupBox *groupBox_2;
    QTableView *tableView;
    QPushButton *pushButton_9;
    QPushButton *pushButton_8;
    QPushButton *pushButton_11;
    QPushButton *pushButton_10;
    QGroupBox *groupBox;
    QLineEdit *lineEdit_4;
    QLineEdit *lineEdit_5;
    QLabel *label;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_3;
    QDateEdit *dateEdit;
    QLabel *label_6;
    QLabel *label_5;
    QLabel *label_4;
    QLabel *label_3;
    QLabel *label_2;
    QPushButton *B1;
    QPushButton *B2;
    QPushButton *B3;
    QPushButton *B4;
    QPushButton *B5;
    QPushButton *B6;
    QPushButton *B7;
    QPushButton *B8;
    QMenuBar *menubar;
    QMenu *menu;
    QMenu *ndirect;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *frmaccueil)
    {
    if (frmaccueil->objectName().isEmpty())
        frmaccueil->setObjectName(QString::fromUtf8("frmaccueil"));
    frmaccueil->resize(700, 615);
    frmaccueil->setMinimumSize(QSize(700, 615));
    frmaccueil->setMaximumSize(QSize(700, 615));
    QFont font;
    font.setPointSize(10);
    frmaccueil->setFont(font);
    frmaccueil->setLayoutDirection(Qt::RightToLeft);
    action = new QAction(frmaccueil);
    action->setObjectName(QString::fromUtf8("action"));
    QFont font1;
    font1.setFamily(QString::fromUtf8("Rasheeq"));
    font1.setPointSize(12);
    action->setFont(font1);
    action_2 = new QAction(frmaccueil);
    action_2->setObjectName(QString::fromUtf8("action_2"));
    action_2->setFont(font1);
    action_3 = new QAction(frmaccueil);
    action_3->setObjectName(QString::fromUtf8("action_3"));
    action_3->setFont(font1);
    action_4 = new QAction(frmaccueil);
    action_4->setObjectName(QString::fromUtf8("action_4"));
    action_4->setFont(font1);
    action_5 = new QAction(frmaccueil);
    action_5->setObjectName(QString::fromUtf8("action_5"));
    action_5->setFont(font1);
    action_6 = new QAction(frmaccueil);
    action_6->setObjectName(QString::fromUtf8("action_6"));
    action_6->setFont(font1);
    action_7 = new QAction(frmaccueil);
    action_7->setObjectName(QString::fromUtf8("action_7"));
    action_7->setFont(font1);
    action_8 = new QAction(frmaccueil);
    action_8->setObjectName(QString::fromUtf8("action_8"));
    action_8->setFont(font1);
    action_9 = new QAction(frmaccueil);
    action_9->setObjectName(QString::fromUtf8("action_9"));
    action_9->setFont(font1);
    centralwidget = new QWidget(frmaccueil);
    centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
    groupBox_2 = new QGroupBox(centralwidget);
    groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
    groupBox_2->setGeometry(QRect(10, 290, 671, 251));
    tableView = new QTableView(groupBox_2);
    tableView->setObjectName(QString::fromUtf8("tableView"));
    tableView->setGeometry(QRect(28, 10, 611, 211));
    QFont font2;
    font2.setFamily(QString::fromUtf8("Rasheeq"));
    font2.setPointSize(11);
    tableView->setFont(font2);
    pushButton_9 = new QPushButton(groupBox_2);
    pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
    pushButton_9->setGeometry(QRect(408, 220, 41, 27));
    pushButton_8 = new QPushButton(groupBox_2);
    pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
    pushButton_8->setGeometry(QRect(368, 220, 41, 27));
    pushButton_11 = new QPushButton(groupBox_2);
    pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
    pushButton_11->setGeometry(QRect(278, 220, 41, 27));
    pushButton_10 = new QPushButton(groupBox_2);
    pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
    pushButton_10->setGeometry(QRect(238, 220, 41, 27));
    groupBox = new QGroupBox(centralwidget);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setGeometry(QRect(31, 10, 621, 231));
    lineEdit_4 = new QLineEdit(groupBox);
    lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
    lineEdit_4->setGeometry(QRect(9, 168, 510, 30));
    lineEdit_4->setFont(font1);
    lineEdit_4->setReadOnly(true);
    lineEdit_5 = new QLineEdit(groupBox);
    lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
    lineEdit_5->setGeometry(QRect(299, 198, 220, 30));
    lineEdit_5->setFont(font1);
    lineEdit_5->setReadOnly(true);
    label = new QLabel(groupBox);
    label->setObjectName(QString::fromUtf8("label"));
    label->setGeometry(QRect(132, 10, 375, 50));
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    sizePolicy.setHorizontalStretch(0);
    sizePolicy.setVerticalStretch(0);
    sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
    label->setSizePolicy(sizePolicy);
    QFont font3;
    font3.setFamily(QString::fromUtf8("Rasheeq"));
    font3.setPointSize(30);
    label->setFont(font3);
    label->setTextFormat(Qt::PlainText);
    label->setScaledContents(true);
    lineEdit = new QLineEdit(groupBox);
    lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
    lineEdit->setGeometry(QRect(299, 80, 220, 30));
    lineEdit->setFont(font1);
    lineEdit->setReadOnly(true);
    lineEdit_3 = new QLineEdit(groupBox);
    lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
    lineEdit_3->setGeometry(QRect(299, 138, 220, 30));
    lineEdit_3->setFont(font1);
    lineEdit_3->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
    lineEdit_3->setReadOnly(true);
    dateEdit = new QDateEdit(groupBox);
    dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
    dateEdit->setGeometry(QRect(298, 108, 220, 29));
    dateEdit->setFont(font2);
    label_6 = new QLabel(groupBox);
    label_6->setObjectName(QString::fromUtf8("label_6"));
    label_6->setGeometry(QRect(524, 196, 93, 30));
    QFont font4;
    font4.setFamily(QString::fromUtf8("Rasheeq"));
    font4.setPointSize(14);
    label_6->setFont(font4);
    label_6->setScaledContents(true);
    label_5 = new QLabel(groupBox);
    label_5->setObjectName(QString::fromUtf8("label_5"));
    label_5->setGeometry(QRect(524, 167, 93, 30));
    label_5->setFont(font4);
    label_5->setScaledContents(true);
    label_4 = new QLabel(groupBox);
    label_4->setObjectName(QString::fromUtf8("label_4"));
    label_4->setGeometry(QRect(524, 138, 93, 30));
    label_4->setFont(font4);
    label_4->setScaledContents(true);
    label_3 = new QLabel(groupBox);
    label_3->setObjectName(QString::fromUtf8("label_3"));
    label_3->setGeometry(QRect(524, 109, 93, 30));
    label_3->setFont(font4);
    label_3->setScaledContents(true);
    label_2 = new QLabel(groupBox);
    label_2->setObjectName(QString::fromUtf8("label_2"));
    label_2->setGeometry(QRect(524, 78, 93, 30));
    QPalette palette;
    QBrush brush(QColor(170, 0, 0, 255));
    brush.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Active, QPalette::Text, brush);
    palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
    QBrush brush1(QColor(119, 119, 115, 255));
    brush1.setStyle(Qt::SolidPattern);
    palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
    label_2->setPalette(palette);
    label_2->setFont(font4);
    label_2->setScaledContents(true);
    B1 = new QPushButton(centralwidget);
    B1->setObjectName(QString::fromUtf8("B1"));
    B1->setGeometry(QRect(603, 250, 80, 27));
    QFont font5;
    font5.setFamily(QString::fromUtf8("Rasheeq"));
    font5.setPointSize(12);
    font5.setBold(false);
    font5.setWeight(50);
    B1->setFont(font5);
    B2 = new QPushButton(centralwidget);
    B2->setObjectName(QString::fromUtf8("B2"));
    B2->setGeometry(QRect(518, 250, 80, 27));
    B2->setFont(font5);
    B3 = new QPushButton(centralwidget);
    B3->setObjectName(QString::fromUtf8("B3"));
    B3->setGeometry(QRect(434, 250, 80, 27));
    B3->setFont(font5);
    B4 = new QPushButton(centralwidget);
    B4->setObjectName(QString::fromUtf8("B4"));
    B4->setEnabled(true);
    B4->setGeometry(QRect(349, 250, 80, 27));
    B4->setFont(font5);
    B5 = new QPushButton(centralwidget);
    B5->setObjectName(QString::fromUtf8("B5"));
    B5->setEnabled(true);
    B5->setGeometry(QRect(265, 250, 80, 27));
    B5->setFont(font5);
    B6 = new QPushButton(centralwidget);
    B6->setObjectName(QString::fromUtf8("B6"));
    B6->setGeometry(QRect(180, 250, 80, 27));
    B6->setFont(font5);
    B7 = new QPushButton(centralwidget);
    B7->setObjectName(QString::fromUtf8("B7"));
    B7->setGeometry(QRect(94, 250, 80, 27));
    B7->setFont(font5);
    B8 = new QPushButton(centralwidget);
    B8->setObjectName(QString::fromUtf8("B8"));
    B8->setGeometry(QRect(7, 250, 80, 27));
    B8->setFont(font5);
    frmaccueil->setCentralWidget(centralwidget);
    menubar = new QMenuBar(frmaccueil);
    menubar->setObjectName(QString::fromUtf8("menubar"));
    menubar->setGeometry(QRect(0, 0, 700, 29));
    menubar->setFont(font1);
    menu = new QMenu(menubar);
    menu->setObjectName(QString::fromUtf8("menu"));
    menu->setFont(font2);
    menu->setLayoutDirection(Qt::RightToLeft);
    ndirect = new QMenu(menubar);
    ndirect->setObjectName(QString::fromUtf8("ndirect"));
    QFont font6;
    font6.setFamily(QString::fromUtf8("Rasheeq"));
    ndirect->setFont(font6);
    frmaccueil->setMenuBar(menubar);
    statusBar = new QStatusBar(frmaccueil);
    statusBar->setObjectName(QString::fromUtf8("statusBar"));
    frmaccueil->setStatusBar(statusBar);
    label_6->setBuddy(lineEdit_5);
    label_5->setBuddy(lineEdit_4);
    label_4->setBuddy(lineEdit_3);
    label_2->setBuddy(lineEdit);

    menubar->addAction(ndirect->menuAction());
    menubar->addAction(menu->menuAction());
    menu->addAction(action);
    menu->addAction(action_2);
    menu->addAction(action_3);
    menu->addAction(action_4);
    menu->addAction(action_5);
    menu->addAction(action_6);
    menu->addAction(action_7);
    menu->addAction(action_8);
    ndirect->addAction(action_9);

    retranslateUi(frmaccueil);

    QMetaObject::connectSlotsByName(frmaccueil);
    } // setupUi

    void retranslateUi(QMainWindow *frmaccueil)
    {
    frmaccueil->setWindowTitle(QApplication::translate("frmaccueil", "\331\205\330\263\330\247\330\271\330\257 \330\247\331\204\331\205\330\261\330\264\330\257 \330\247\331\204\330\252\330\261\330\250\331\210\331\212", 0, QApplication::UnicodeUTF8));
    action->setText(QApplication::translate("frmaccueil", "\330\264\331\207\330\247\330\257\330\251 \330\255\330\266\331\210\330\261", 0, QApplication::UnicodeUTF8));
    action_2->setText(QApplication::translate("frmaccueil", "\331\205\330\260\330\247\331\203\330\261\330\251 \330\252\331\203\331\205\331\212\331\204\331\212\330\251", 0, QApplication::UnicodeUTF8));
    action_3->setText(QApplication::translate("frmaccueil", "\330\245\330\263\330\252\330\257\330\271\330\247\330\241 \331\210\331\204\331\212", 0, QApplication::UnicodeUTF8));
    action_4->setText(QApplication::translate("frmaccueil", "\330\245\331\206\330\260\330\247\330\261", 0, QApplication::UnicodeUTF8));
    action_5->setText(QApplication::translate("frmaccueil", "\330\245\330\271\331\204\330\247\331\205 \330\250\330\247\331\204\330\264\330\267\330\250", 0, QApplication::UnicodeUTF8));
    action_6->setText(QApplication::translate("frmaccueil", "\330\245\330\271\331\204\330\247\331\205 \330\250\330\247\331\204\330\272\331\212\330\247\330\250", 0, QApplication::UnicodeUTF8));
    action_7->setText(QApplication::translate("frmaccueil", "\330\261\331\201\330\252 \331\205\330\244\331\202\330\252", 0, QApplication::UnicodeUTF8));
    action_8->setText(QApplication::translate("frmaccueil", "\330\245\330\271\331\204\330\247\331\205 \330\250\330\255\330\247\330\257\330\253", 0, QApplication::UnicodeUTF8));
    action_9->setText(QApplication::translate("frmaccueil", "\330\252\330\255\330\257\331\212\330\257 \330\247\330\263\331\205 \330\247\331\204\331\205\330\257\331\212\330\261", 0, QApplication::UnicodeUTF8));
    groupBox_2->setTitle(QString());
    pushButton_9->setText(QApplication::translate("frmaccueil", "<<", 0, QApplication::UnicodeUTF8));
    pushButton_8->setText(QApplication::translate("frmaccueil", "<", 0, QApplication::UnicodeUTF8));
    pushButton_11->setText(QApplication::translate("frmaccueil", ">", 0, QApplication::UnicodeUTF8));
    pushButton_10->setText(QApplication::translate("frmaccueil", ">>", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QString());
    label->setText(QApplication::translate("frmaccueil", "\331\205\330\263\330\247\330\271\330\257 \330\247\331\204\331\205\330\261\330\264\330\257 \330\247\331\204\330\252\330\261\330\250\331\210\331\212", 0, QApplication::UnicodeUTF8));
    label_6->setText(QApplication::translate("frmaccueil", "\330\245\330\263\331\205 \330\247\331\204\331\210\331\204\331\212", 0, QApplication::UnicodeUTF8));
    label_5->setText(QApplication::translate("frmaccueil", "\330\247\331\204\330\271\331\206\331\210\330\247\331\206", 0, QApplication::UnicodeUTF8));
    label_4->setText(QApplication::translate("frmaccueil", "\330\247\331\204\331\202\330\263\331\205", 0, QApplication::UnicodeUTF8));
    label_3->setText(QApplication::translate("frmaccueil", "\330\252\330\247\330\261\331\212\330\256 \330\247\331\204\331\210\331\204\330\247\330\257\330\251", 0, QApplication::UnicodeUTF8));
    label_2->setText(QApplication::translate("frmaccueil", "\330\247\331\204\330\245\330\263\331\205 \331\210 \330\247\331\204\331\204\331\202\330\250", 0, QApplication::UnicodeUTF8));
    B1->setText(QApplication::translate("frmaccueil", "\330\245\330\266\330\247\331\201\330\251", 0, QApplication::UnicodeUTF8));
    B2->setText(QApplication::translate("frmaccueil", "\330\252\330\272\331\212\331\212\330\261", 0, QApplication::UnicodeUTF8));
    B3->setText(QApplication::translate("frmaccueil", "\330\255\330\260\331\201", 0, QApplication::UnicodeUTF8));
    B4->setText(QApplication::translate("frmaccueil", "\330\252\330\263\330\254\331\212\331\204", 0, QApplication::UnicodeUTF8));
    B5->setText(QApplication::translate("frmaccueil", "\330\245\331\204\330\272\330\247\330\241", 0, QApplication::UnicodeUTF8));
    B6->setText(QApplication::translate("frmaccueil", "\330\250\330\255\330\253", 0, QApplication::UnicodeUTF8));
    B7->setText(QApplication::translate("frmaccueil", "\330\255\330\257\330\253", 0, QApplication::UnicodeUTF8));
    B8->setText(QApplication::translate("frmaccueil", "\330\256\330\261\331\210\330\254", 0, QApplication::UnicodeUTF8));
    menu->setTitle(QApplication::translate("frmaccueil", "\330\264\330\244\331\210\331\206 \330\247\331\204\330\252\331\204\330\247\331\205\331\212\330\260", 0, QApplication::UnicodeUTF8));
    ndirect->setTitle(QApplication::translate("frmaccueil", "\330\247\330\263\331\205 \330\247\331\204\331\205\330\257\331\212\330\261", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class frmaccueil: public Ui_frmaccueil {};
} // namespace Ui

#endif // UI_ACCUEIL_H
