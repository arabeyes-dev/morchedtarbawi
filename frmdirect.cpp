/* Morched Tarbawi
* Copyright (c) 2007 Wathek LOUED.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "frmdirect.h"
#include "frmaccueil.h"
#include <QtSql>
#include <QLocale>
#include <QString>

frmdirect::frmdirect(QSqlQueryModel *model,QWidget *parent)
        :QDialog(parent), m_model(model)
{
        setupUi(this);
	model2 = new QSqlQueryModel(this);
	model2->setQuery("select * from ndirect");
	lineEdit->setReadOnly(true);
	lineEdit->setText(model2->record(0).value("nom").toString());

	connect(pushButton, SIGNAL(clicked()), this, SLOT(close()));
	connect(pushButton_2, SIGNAL(clicked()), this, SLOT(appliquer()));
}

void frmdirect::appliquer()
{
	QSqlQuery query;
        query.prepare("UPDATE ndirect set nom=:nom where nom=:nomanc");
        query.bindValue(":nom", lineEdit_2->text());
	query.bindValue(":nomanc", lineEdit->text());
	query.exec();
	this->close();
}
