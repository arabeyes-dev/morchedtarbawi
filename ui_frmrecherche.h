/********************************************************************************
** Form generated from reading ui file 'frmrecherche.ui'
**
** Created: Thu Feb 7 22:52:43 2008
**      by: Qt User Interface Compiler version 4.3.2
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_FRMRECHERCHE_H
#define UI_FRMRECHERCHE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDateEdit>
#include <QtGui/QDialog>
#include <QtGui/QGroupBox>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>

class Ui_frmrecherche
{
public:
    QGroupBox *groupBox;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QLineEdit *lineEdit;
    QComboBox *comboBox;
    QDateEdit *dateEdit;

    void setupUi(QDialog *frmrecherche)
    {
    if (frmrecherche->objectName().isEmpty())
        frmrecherche->setObjectName(QString::fromUtf8("frmrecherche"));
    frmrecherche->setEnabled(true);
    frmrecherche->resize(521, 199);
    frmrecherche->setMinimumSize(QSize(521, 199));
    frmrecherche->setMaximumSize(QSize(521, 254));
    frmrecherche->setLayoutDirection(Qt::RightToLeft);
    frmrecherche->setSizeGripEnabled(false);
    groupBox = new QGroupBox(frmrecherche);
    groupBox->setObjectName(QString::fromUtf8("groupBox"));
    groupBox->setGeometry(QRect(10, 19, 501, 171));
    pushButton = new QPushButton(groupBox);
    pushButton->setObjectName(QString::fromUtf8("pushButton"));
    pushButton->setGeometry(QRect(20, 131, 80, 27));
    QFont font;
    font.setPointSize(11);
    pushButton->setFont(font);
    pushButton_2 = new QPushButton(groupBox);
    pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
    pushButton_2->setGeometry(QRect(100, 131, 80, 27));
    pushButton_2->setFont(font);
    radioButton = new QRadioButton(groupBox);
    radioButton->setObjectName(QString::fromUtf8("radioButton"));
    radioButton->setGeometry(QRect(382, 21, 101, 23));
    QFont font1;
    font1.setPointSize(12);
    radioButton->setFont(font1);
    radioButton_2 = new QRadioButton(groupBox);
    radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));
    radioButton_2->setGeometry(QRect(342, 56, 141, 23));
    radioButton_2->setFont(font1);
    radioButton_3 = new QRadioButton(groupBox);
    radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));
    radioButton_3->setGeometry(QRect(372, 89, 111, 27));
    radioButton_3->setFont(font1);
    lineEdit = new QLineEdit(groupBox);
    lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
    lineEdit->setGeometry(QRect(80, 14, 241, 27));
    lineEdit->setFont(font);
    comboBox = new QComboBox(groupBox);
    comboBox->setObjectName(QString::fromUtf8("comboBox"));
    comboBox->setGeometry(QRect(80, 88, 241, 26));
    comboBox->setFont(font);
    dateEdit = new QDateEdit(groupBox);
    dateEdit->setObjectName(QString::fromUtf8("dateEdit"));
    dateEdit->setGeometry(QRect(80, 51, 241, 27));
    dateEdit->setFont(font);

    retranslateUi(frmrecherche);

    QMetaObject::connectSlotsByName(frmrecherche);
    } // setupUi

    void retranslateUi(QDialog *frmrecherche)
    {
    frmrecherche->setWindowTitle(QApplication::translate("frmrecherche", "\330\250\330\255\330\253", 0, QApplication::UnicodeUTF8));
    groupBox->setTitle(QString());
    pushButton->setText(QApplication::translate("frmrecherche", "\330\245\331\204\330\272\330\247\330\241", 0, QApplication::UnicodeUTF8));
    pushButton_2->setText(QApplication::translate("frmrecherche", "\330\250\330\255\330\253", 0, QApplication::UnicodeUTF8));
    radioButton->setText(QApplication::translate("frmrecherche", "\330\255\330\263\330\250 \330\247\331\204\330\245\330\263\331\205", 0, QApplication::UnicodeUTF8));
    radioButton_2->setText(QApplication::translate("frmrecherche", "\330\255\330\263\330\250 \330\252\330\247\330\261\331\212\330\256 \330\247\331\204\331\210\331\204\330\247\330\257\330\251", 0, QApplication::UnicodeUTF8));
    radioButton_3->setText(QApplication::translate("frmrecherche", "\330\255\330\263\330\250 \330\247\331\204\331\202\330\263\331\205", 0, QApplication::UnicodeUTF8));
    Q_UNUSED(frmrecherche);
    } // retranslateUi

};

namespace Ui {
    class frmrecherche: public Ui_frmrecherche {};
} // namespace Ui

#endif // UI_FRMRECHERCHE_H
